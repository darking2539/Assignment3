EESchema Schematic File Version 2
LIBS:HG_device
LIBS:Amplifier_Audio
LIBS:Amplifier_Buffer
LIBS:Amplifier_Current
LIBS:Amplifier_Difference
LIBS:Amplifier_Instrumentation
LIBS:Amplifier_Operational
LIBS:Amplifier_Video
LIBS:Analog
LIBS:Analog_ADC
LIBS:Analog_DAC
LIBS:Analog_Switch
LIBS:Audio
LIBS:Battery_Management
LIBS:Comparator
LIBS:Connector_Generic
LIBS:Connector_Generic_Shielded
LIBS:Connector_Specialized
LIBS:Converter_DCDC
LIBS:CPLD_Altera
LIBS:CPLD_Xilinx
LIBS:CPU
LIBS:Device
LIBS:Diode
LIBS:Diode_Laser
LIBS:Display_Character
LIBS:Display_Graphic
LIBS:Driver_Display
LIBS:Driver_FET
LIBS:Driver_LED
LIBS:Driver_Motor
LIBS:Driver_Relay
LIBS:DSP_Freescale
LIBS:DSP_Microchip_DSPIC33
LIBS:DSP_Texas
LIBS:FPGA_Actel
LIBS:FPGA_Xilinx
LIBS:FPGA_Xilinx_Artix7
LIBS:FPGA_Xilinx_Kintex7
LIBS:FPGA_Xilinx_Spartan6
LIBS:FPGA_Xilinx_Virtex5
LIBS:FPGA_Xilinx_Virtex6
LIBS:FPGA_Xilinx_Virtex7
LIBS:Graphic
LIBS:Interface
LIBS:Interface_CAN_LIN
LIBS:Interface_CurrentLoop
LIBS:Interface_Ethernet
LIBS:Interface_Expansion
LIBS:Interface_HID
LIBS:Interface_LineDriver
LIBS:Interface_Optical
LIBS:Interface_Telecom
LIBS:Interface_UART
LIBS:Interface_USB
LIBS:Isolator
LIBS:Isolator_Analog
LIBS:Jumper
LIBS:LED
LIBS:Logic_74xgxx
LIBS:Logic_74xx
LIBS:Logic_CMOS_4000
LIBS:Logic_CMOS_IEEE
LIBS:Logic_LevelTranslator
LIBS:Logic_Programmable
LIBS:Logic_TTL_IEEE
LIBS:MCU_AnalogDevices
LIBS:MCU_Atmel_8051
LIBS:MCU_Atmel_ATMEGA
LIBS:MCU_Atmel_ATTINY
LIBS:MCU_Atmel_AVR
LIBS:MCU_Cypress
LIBS:MCU_Infineon
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_Microchip_SAME
LIBS:MCU_Microchip_SAML
LIBS:MCU_NXP_HC11
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_SiFive
LIBS:MCU_SiliconLabs
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32
LIBS:MCU_Texas
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:Memory_Controller
LIBS:Memory_EEPROM
LIBS:Memory_Flash
LIBS:Memory_NVRAM
LIBS:Memory_RAM
LIBS:Memory_ROM
LIBS:Memory_UniqueID
LIBS:Motor
LIBS:Oscillator
LIBS:Potentiometer_Digital
LIBS:power
LIBS:Power_Management
LIBS:Power_Protection
LIBS:Power_Supervisor
LIBS:pspice
LIBS:Reference_Current
LIBS:Reference_Voltage
LIBS:Regulator_Controller
LIBS:Regulator_Current
LIBS:Regulator_Linear
LIBS:Regulator_SwitchedCapacitor
LIBS:Regulator_Switching
LIBS:Relay
LIBS:Relay_SolidState
LIBS:RF_AM_FM
LIBS:RF_Bluetooth
LIBS:RF_Mixer
LIBS:RF_Module
LIBS:RF_RFID
LIBS:RF_WiFi
LIBS:Sensor
LIBS:Sensor_Audio
LIBS:Sensor_Current
LIBS:Sensor_Gas
LIBS:Sensor_Humidity
LIBS:Sensor_Magnetic
LIBS:Sensor_Motion
LIBS:Sensor_MultiFunction
LIBS:Sensor_Optical
LIBS:Sensor_Pressure
LIBS:Sensor_Proximity
LIBS:Sensor_Temperature
LIBS:Sensor_Touch
LIBS:Sensor_Voltage
LIBS:Switch
LIBS:Timer
LIBS:Timer_RTC
LIBS:Transformer
LIBS:Transistor_Array
LIBS:Transistor_BJT
LIBS:Transistor_FET
LIBS:Transistor_IGBT
LIBS:Valve
LIBS:Video
LIBS:arduino_shieldsNCL
LIBS:auv
LIBS:HG_74xx
LIBS:HG_BMS
LIBS:HG_regulators
LIBS:usb_dnt24c-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "USB2UART-picoblade6pin"
Date "2018-02-19"
Rev "2.0"
Comp "HiveGround Co., Ltd."
Comment1 "VCCIO = 3.3V"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R2
U 1 1 543E41B9
P 7100 1400
F 0 "R2" V 7180 1400 40  0000 C CNN
F 1 "270R" V 7107 1401 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7030 1400 30  0001 C CNN
F 3 "" H 7100 1400 30  0000 C CNN
F 4 "CRCW0603270RFKEA" H 7100 1400 60  0001 C CNN "partno"
	1    7100 1400
	1    0    0    -1  
$EndComp
$Comp
L FILTER FB1
U 1 1 543E438B
P 2200 1500
F 0 "FB1" H 2200 1650 60  0000 C CNN
F 1 "FILTER" H 2200 1400 60  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 2200 1500 60  0001 C CNN
F 3 "" H 2200 1500 60  0000 C CNN
F 4 "MPZ2012S101AT-CT" H 2200 1500 60  0001 C CNN "partno"
	1    2200 1500
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 543E462B
P 6750 1400
F 0 "R1" V 6830 1400 40  0000 C CNN
F 1 "270R" V 6757 1401 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6680 1400 30  0001 C CNN
F 3 "" H 6750 1400 30  0000 C CNN
F 4 "CRCW0603270RFKEA" H 6750 1400 60  0001 C CNN "partno"
	1    6750 1400
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 543E4787
P 2650 2300
F 0 "C2" H 2650 2400 40  0000 L CNN
F 1 "100nF" H 2656 2215 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2688 2150 30  0001 C CNN
F 3 "" H 2650 2300 60  0000 C CNN
F 4 "MC0603B104K500CT" H 2650 2300 60  0001 C CNN "partno"
	1    2650 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 543E47B2
P 4050 3750
F 0 "#PWR01" H 4050 3750 30  0001 C CNN
F 1 "GND" H 4050 3680 30  0001 C CNN
F 2 "" H 4050 3750 60  0000 C CNN
F 3 "" H 4050 3750 60  0000 C CNN
	1    4050 3750
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 543E47CD
P 2650 1350
F 0 "#PWR02" H 2650 1450 30  0001 C CNN
F 1 "VCC" H 2650 1450 30  0000 C CNN
F 2 "" H 2650 1350 60  0000 C CNN
F 3 "" H 2650 1350 60  0000 C CNN
	1    2650 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 543E47ED
P 2500 2650
F 0 "#PWR03" H 2500 2650 30  0001 C CNN
F 1 "GND" H 2500 2580 30  0001 C CNN
F 2 "" H 2500 2650 60  0000 C CNN
F 3 "" H 2500 2650 60  0000 C CNN
	1    2500 2650
	1    0    0    -1  
$EndComp
$Comp
L CP1 C1
U 1 1 543E4829
P 1850 2300
F 0 "C1" H 1900 2400 50  0000 L CNN
F 1 "10nF" H 1900 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1850 2300 60  0001 C CNN
F 3 "" H 1850 2300 60  0000 C CNN
F 4 "MC0603B103K500CT" H 1850 2300 60  0001 C CNN "partno"
	1    1850 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 543E505F
P 7100 1100
F 0 "#PWR04" H 7100 1200 30  0001 C CNN
F 1 "VCC" H 7100 1200 30  0000 C CNN
F 2 "" H 7100 1100 60  0000 C CNN
F 3 "" H 7100 1100 60  0000 C CNN
	1    7100 1100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 543E4817
P 1750 2650
F 0 "#PWR05" H 1750 2650 30  0001 C CNN
F 1 "GND" H 1750 2580 30  0001 C CNN
F 2 "" H 1750 2650 60  0000 C CNN
F 3 "" H 1750 2650 60  0000 C CNN
	1    1750 2650
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 54AF5A34
P 2850 3250
F 0 "C3" H 2850 3350 40  0000 L CNN
F 1 "100nF" H 2856 3165 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2888 3100 30  0001 C CNN
F 3 "" H 2850 3250 60  0000 C CNN
F 4 "MC0603B104K500CT" H 2850 3250 60  0001 C CNN "partno"
	1    2850 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 54AF5A99
P 2850 3450
F 0 "#PWR06" H 2850 3450 30  0001 C CNN
F 1 "GND" H 2850 3380 30  0001 C CNN
F 2 "" H 2850 3450 60  0000 C CNN
F 3 "" H 2850 3450 60  0000 C CNN
	1    2850 3450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C4
U 1 1 54B0D361
P 2250 2300
F 0 "C4" H 2300 2400 50  0000 L CNN
F 1 "4.7uF" H 2300 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2250 2300 60  0001 C CNN
F 3 "" H 2250 2300 60  0000 C CNN
F 4 "MC1206B475K250" H 2250 2300 60  0001 C CNN "partno"
	1    2250 2300
	1    0    0    -1  
$EndComp
Text Label 5300 1250 0    60   ~ 0
FT232_TX
Text Label 5300 1400 0    60   ~ 0
FT232_RX
$Comp
L LED_Small D1
U 1 1 5A53E01E
P 6750 1950
F 0 "D1" H 6700 2075 50  0000 L CNN
F 1 "LED Green" H 6575 1850 50  0000 L CNN
F 2 "HG_Devices:LED_0603" V 6750 1950 50  0001 C CNN
F 3 "" V 6750 1950 50  0001 C CNN
F 4 "LTST-C190KGKT" H 6750 1950 60  0001 C CNN "partno"
	1    6750 1950
	0    -1   -1   0   
$EndComp
$Comp
L LED_Small D2
U 1 1 5A53E3FD
P 7100 1950
F 0 "D2" H 7050 2075 50  0000 L CNN
F 1 "LED Orange" H 6925 1850 50  0000 L CNN
F 2 "HG_Devices:LED_0603" V 7100 1950 50  0001 C CNN
F 3 "" V 7100 1950 50  0001 C CNN
F 4 "LTST-C190KFKT" H 7100 1950 60  0001 C CNN "partno"
	1    7100 1950
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5A8A6709
P 2850 3000
F 0 "#PWR07" H 2850 2850 50  0001 C CNN
F 1 "+3.3V" H 2850 3140 50  0000 C CNN
F 2 "" H 2850 3000 50  0000 C CNN
F 3 "" H 2850 3000 50  0000 C CNN
	1    2850 3000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR08
U 1 1 5A8A674D
P 2850 3000
F 0 "#PWR08" H 2850 2850 50  0001 C CNN
F 1 "+3.3V" H 2850 3140 50  0000 C CNN
F 2 "" H 2850 3000 50  0000 C CNN
F 3 "" H 2850 3000 50  0000 C CNN
	1    2850 3000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR09
U 1 1 5A8A6765
P 2850 1250
F 0 "#PWR09" H 2850 1100 50  0001 C CNN
F 1 "+3.3V" H 2850 1390 50  0000 C CNN
F 2 "" H 2850 1250 50  0000 C CNN
F 3 "" H 2850 1250 50  0000 C CNN
	1    2850 1250
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5A8A7330
P 5450 2900
F 0 "R3" V 5530 2900 40  0000 C CNN
F 1 "10k" V 5457 2901 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5380 2900 30  0001 C CNN
F 3 "" H 5450 2900 30  0000 C CNN
F 4 "ERJ-3EKF1002V" H 5450 2900 60  0001 C CNN "partno"
	1    5450 2900
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR010
U 1 1 5A8A739B
P 5800 2900
F 0 "#PWR010" H 5800 2750 50  0001 C CNN
F 1 "+3.3V" H 5800 3040 50  0000 C CNN
F 2 "" H 5800 2900 50  0000 C CNN
F 3 "" H 5800 2900 50  0000 C CNN
	1    5800 2900
	1    0    0    -1  
$EndComp
Text Label 1700 1500 0    60   ~ 0
Vbus
$Comp
L FT232RL U2
U 1 1 5B16705D
P 4050 2150
F 0 "U2" H 3300 3350 60  0000 C CNN
F 1 "FT232RL" H 4050 2150 60  0000 C CNN
F 2 "Housings_SSOP:SSOP-28_5.3x10.2mm_Pitch0.65mm" H 4050 2150 60  0001 C CNN
F 3 "" H 4050 2150 60  0001 C CNN
F 4 "FT232RL-REEL" H 4050 2150 60  0001 C CNN "partno"
	1    4050 2150
	1    0    0    -1  
$EndComp
$Comp
L USB_OTG J1
U 1 1 5B168367
P 1100 1700
F 0 "J1" H 900 2150 50  0000 L CNN
F 1 "USB_OTG" H 900 2050 50  0000 L CNN
F 2 "Connectors_USB:USB_Micro-B_Molex_47346-0001" H 1250 1650 50  0001 C CNN
F 3 "" H 1250 1650 50  0001 C CNN
F 4 "47346-0001" H 1100 1700 60  0001 C CNN "partno"
	1    1100 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5B16AC80
P 1000 2150
F 0 "#PWR011" H 1000 2150 30  0001 C CNN
F 1 "GND" H 1000 2080 30  0001 C CNN
F 2 "" H 1000 2150 60  0000 C CNN
F 3 "" H 1000 2150 60  0000 C CNN
	1    1000 2150
	1    0    0    -1  
$EndComp
Text Notes 2850 1700 0    60   ~ 0
D-
Text Notes 2850 1850 0    60   ~ 0
D+
$Comp
L DNT24P U1
U 1 1 5B176811
P 8450 4600
F 0 "U1" H 7600 5700 60  0000 C CNN
F 1 "DNT24C" H 8450 4550 60  0000 C CNN
F 2 "HG_Devices:DNT24C_v2" H 7500 5600 60  0001 C CNN
F 3 "" H 7600 5700 60  0001 C CNN
F 4 "DNT24C" H 7700 5800 60  0001 C CNN "partno"
	1    8450 4600
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 5B17A8F5
P 6750 5100
F 0 "C11" H 6775 5200 50  0000 L CNN
F 1 "100nF" H 6775 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6788 4950 50  0001 C CNN
F 3 "" H 6750 5100 50  0001 C CNN
F 4 "MC0603B104K500CT" H 6750 5100 60  0001 C CNN "partno"
	1    6750 5100
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 5B17A9DB
P 7100 5100
F 0 "C12" H 7125 5200 50  0000 L CNN
F 1 "10nF" H 7125 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7138 4950 50  0001 C CNN
F 3 "" H 7100 5100 50  0001 C CNN
F 4 "MC0603B103K500CT" H 7100 5100 60  0001 C CNN "partno"
	1    7100 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5B17B345
P 5550 5300
F 0 "#PWR012" H 5550 5300 30  0001 C CNN
F 1 "GND" H 5550 5230 30  0001 C CNN
F 2 "" H 5550 5300 60  0000 C CNN
F 3 "" H 5550 5300 60  0000 C CNN
	1    5550 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5B17C9C9
P 7050 3650
F 0 "#PWR013" H 7050 3650 30  0001 C CNN
F 1 "GND" H 7050 3580 30  0001 C CNN
F 2 "" H 7050 3650 60  0000 C CNN
F 3 "" H 7050 3650 60  0000 C CNN
	1    7050 3650
	0    1    1    0   
$EndComp
$Comp
L GND #PWR014
U 1 1 5B17D02E
P 9850 5100
F 0 "#PWR014" H 9850 5100 30  0001 C CNN
F 1 "GND" H 9850 5030 30  0001 C CNN
F 2 "" H 9850 5100 60  0000 C CNN
F 3 "" H 9850 5100 60  0000 C CNN
	1    9850 5100
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06 J2
U 1 1 5B1A4E74
P 5100 3750
F 0 "J2" H 5100 4050 50  0000 C CNN
F 1 "Conn_01x06" H 5100 3350 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53261-0671_06x1.25mm_Angled" H 5100 3750 50  0001 C CNN
F 3 "" H 5100 3750 50  0001 C CNN
F 4 "53261-0671" H 5100 3750 60  0001 C CNN "partno"
	1    5100 3750
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x06 J3
U 1 1 5B1A4FAE
P 5100 4800
F 0 "J3" H 5100 5100 50  0000 C CNN
F 1 "Conn_01x06" H 5100 4400 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53261-0671_06x1.25mm_Angled" H 5100 4800 50  0001 C CNN
F 3 "" H 5100 4800 50  0001 C CNN
F 4 "53261-0671" H 5100 4800 60  0001 C CNN "partno"
	1    5100 4800
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5B1E0BB2
P 5300 4050
F 0 "#PWR015" H 5300 4050 30  0001 C CNN
F 1 "GND" H 5300 3980 30  0001 C CNN
F 2 "" H 5300 4050 60  0000 C CNN
F 3 "" H 5300 4050 60  0000 C CNN
	1    5300 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5B1E0D10
P 5300 5100
F 0 "#PWR016" H 5300 5100 30  0001 C CNN
F 1 "GND" H 5300 5030 30  0001 C CNN
F 2 "" H 5300 5100 60  0000 C CNN
F 3 "" H 5300 5100 60  0000 C CNN
	1    5300 5100
	1    0    0    -1  
$EndComp
Text Notes 5350 4700 0    60   ~ 0
Pixhawk_TX
Text Notes 5350 4800 0    60   ~ 0
Pixhawk_RX
Text Notes 5350 3650 0    60   ~ 0
Pixhawk_TX
Wire Wire Line
	1650 2100 1650 2500
Wire Wire Line
	1750 2650 1750 2500
Connection ~ 1750 2500
Wire Wire Line
	1650 2500 1850 2500
Wire Wire Line
	1400 1500 1850 1500
Wire Wire Line
	2550 1500 2850 1500
Wire Wire Line
	2850 1400 2950 1400
Wire Wire Line
	2650 1350 2650 2150
Connection ~ 2650 1500
Wire Wire Line
	2850 1250 2950 1250
Wire Wire Line
	6750 1550 6750 1850
Wire Wire Line
	7100 1550 7100 1850
Wire Wire Line
	7100 1100 7100 1250
Connection ~ 7100 1150
Connection ~ 1850 1500
Wire Wire Line
	4050 3550 4050 3750
Wire Wire Line
	3750 3550 3750 3650
Wire Wire Line
	3750 3650 4350 3650
Connection ~ 4050 3650
Wire Wire Line
	3900 3550 3900 3650
Connection ~ 3900 3650
Wire Wire Line
	4200 3650 4200 3550
Wire Wire Line
	4350 3650 4350 3550
Connection ~ 4200 3650
Wire Wire Line
	2650 2100 2250 2100
Wire Wire Line
	5150 1250 6100 1250
Wire Wire Line
	6750 1150 7100 1150
Connection ~ 2650 2100
Wire Wire Line
	2250 2500 2650 2500
Wire Wire Line
	2500 2500 2500 2650
Connection ~ 2500 2500
Wire Wire Line
	6750 1150 6750 1250
Wire Wire Line
	1850 2500 1850 2450
Wire Wire Line
	2250 2100 2250 2150
Wire Wire Line
	2650 2500 2650 2450
Wire Wire Line
	2250 2500 2250 2450
Wire Wire Line
	2850 1500 2850 1400
Wire Wire Line
	2050 1850 2950 1850
Wire Wire Line
	2850 3000 2850 3100
Wire Wire Line
	2850 3050 2950 3050
Wire Wire Line
	2850 3450 2850 3400
Connection ~ 2850 3050
Wire Wire Line
	5150 2900 5300 2900
Wire Wire Line
	5600 2900 5800 2900
Wire Wire Line
	1000 2100 1650 2100
Wire Wire Line
	1000 2100 1000 2150
Wire Wire Line
	2050 1850 2050 1700
Wire Wire Line
	2050 1700 1400 1700
Wire Wire Line
	1400 1800 2450 1800
Wire Wire Line
	2450 1800 2450 1700
Wire Wire Line
	2450 1700 2950 1700
Connection ~ 1100 2100
Wire Wire Line
	7100 2050 7100 2600
Wire Wire Line
	6750 2050 6750 2450
Wire Wire Line
	7100 2600 5150 2600
Wire Wire Line
	6750 2450 5150 2450
Wire Wire Line
	6600 4950 7350 4950
Connection ~ 7100 4950
Wire Wire Line
	6600 4950 6600 4900
Connection ~ 6750 4950
Wire Wire Line
	6750 5250 7350 5250
Wire Wire Line
	7350 5250 7350 5050
Connection ~ 7100 5250
Wire Wire Line
	7350 3650 7050 3650
Wire Wire Line
	9550 3650 9850 3650
Wire Wire Line
	9850 3650 9850 5100
Wire Wire Line
	9850 3850 9550 3850
Wire Wire Line
	9850 5050 9550 5050
Connection ~ 9850 3850
Wire Wire Line
	10150 4950 9550 4950
Wire Wire Line
	5700 4150 7350 4150
Wire Wire Line
	5150 1400 6200 1400
Wire Wire Line
	6200 1400 6200 4800
Connection ~ 9850 5050
Wire Wire Line
	5700 4700 5300 4700
Connection ~ 6200 4050
Wire Wire Line
	6200 4800 5300 4800
Connection ~ 6100 4150
Wire Wire Line
	6100 1250 6100 4150
Wire Wire Line
	6200 4050 7350 4050
Wire Wire Line
	5700 4700 5700 4150
Text Notes 5350 3750 0    60   ~ 0
Pixhawk_RX
Wire Wire Line
	5300 3750 7350 3750
Wire Wire Line
	7350 3850 6550 3850
Wire Wire Line
	6550 3850 6550 3650
Wire Wire Line
	6550 3650 5300 3650
$Comp
L +5V #PWR017
U 1 1 5B1E37F8
P 5300 4600
F 0 "#PWR017" H 5300 4450 50  0001 C CNN
F 1 "+5V" H 5300 4740 50  0000 C CNN
F 2 "" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1200 1850 2150
$Comp
L +5V #PWR018
U 1 1 5B1DFB4F
P 1850 1200
F 0 "#PWR018" H 1850 1050 50  0001 C CNN
F 1 "+5V" H 1850 1340 50  0000 C CNN
F 2 "" H 1850 1200 50  0001 C CNN
F 3 "" H 1850 1200 50  0001 C CNN
	1    1850 1200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR019
U 1 1 5B1F7911
P 6600 4900
F 0 "#PWR019" H 6600 5000 30  0001 C CNN
F 1 "VCC" H 6600 5000 30  0000 C CNN
F 2 "" H 6600 4900 60  0000 C CNN
F 3 "" H 6600 4900 60  0000 C CNN
	1    6600 4900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR020
U 1 1 5B1F794A
P 10150 4950
F 0 "#PWR020" H 10150 5050 30  0001 C CNN
F 1 "VCC" H 10150 5050 30  0000 C CNN
F 2 "" H 10150 4950 60  0000 C CNN
F 3 "" H 10150 4950 60  0000 C CNN
	1    10150 4950
	1    0    0    -1  
$EndComp
$Comp
L CP1 C5
U 1 1 5B1F7DA6
P 5550 5150
F 0 "C5" H 5600 5250 50  0000 L CNN
F 1 "10nF" H 5600 5050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5550 5150 60  0001 C CNN
F 3 "" H 5550 5150 60  0000 C CNN
F 4 "MC0603B103K500CT" H 5550 5150 60  0001 C CNN "partno"
	1    5550 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4600 5550 4600
Wire Wire Line
	5550 4600 5550 5000
$Comp
L GND #PWR021
U 1 1 5B1FB041
P 7100 5250
F 0 "#PWR021" H 7100 5250 30  0001 C CNN
F 1 "GND" H 7100 5180 30  0001 C CNN
F 2 "" H 7100 5250 60  0000 C CNN
F 3 "" H 7100 5250 60  0000 C CNN
	1    7100 5250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
